#!/usr/bin/env bash

NEW_UUID=$(cat /dev/urandom | env LC_CTYPE=C tr -cd 'a-f0-9' | head -c 64)
UPDATER_TOKEN="updater-token.php"

echo "<?php return [\"token\" => \"${NEW_UUID}\"];" > ${UPDATER_TOKEN}
echo "New acces token generated in ${UPDATER_TOKEN}"
