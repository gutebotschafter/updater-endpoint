<?php

namespace Http;

/**
 * Class JsonResponse
 *
 * @package Http
 */
class JsonResponse
{
    /** @var array */
    private $json;

    /**
     * JsonResponse constructor.
     * @param array $data
     */
    public function __construct($data = [])
    {
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json; charset=UTF-8');
        http_response_code($data["code"]);

        $this->json = $data;

        die(json_encode($this->json, JSON_UNESCAPED_UNICODE));
    }
}
