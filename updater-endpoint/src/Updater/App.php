<?php /** @noinspection PhpIncludeInspection */

namespace Updater;

use Exception;
use Http\JsonResponse;
use Database\DbReplicator;
use Database\DbTableInfo;
use Yaml\Yaml;

/**
 * Class App
 *
 * @package Updater
 */
class App
{
    /** @var array */
    private $configuration = [];

    /** @var array */
    private $connection = [];

    /** @var Yaml */
    private $yml;

    /** @var string */
    private $webroot = "public";

    /**
     * App constructor.
     *
     * @param array $configuration
     */
    public function __construct(array $configuration) {
        $this->configuration = $configuration;

        $file = __DIR__ . "/../../../updater.yml";

        if (file_exists($file)) {
            $this->yml = new Yaml($file);
            $array = $this->yml->getArray();

            if (array_key_exists("webroot", $array)) {
                $this->webroot = $array["webroot"];
            }
        }

        $this->prepareConfiguration();

        if (!file_exists($this->configuration["UPDATER_CONFIG"])) {
            $props = [
                "status" => "failed",
                "message" => "Configuration Token not found",
                "code" => 500
            ];

            return new JsonResponse($props);
        } else {
            $token = include($this->configuration["UPDATER_CONFIG"]);
        }

        if ($this->configuration["requestToken"] === null || $this->configuration["requestToken"] !== $token["token"]) {
            $props = [
                "status" => "Forbidden",
                "message" => "Wrong token Authentication!",
                "code" => 403
            ];

            return new JsonResponse($props);
        }

        if (file_exists($this->configuration["TYPO_CONFIG"])) {
            $configFile = include($this->configuration["TYPO_CONFIG"]);
            $config = $configFile["DB"]["Connections"]["Default"];

            $this->connection = [
                "SERVER" => $config["host"],
                "DATABASE" => $config["dbname"],
                "USER" => $config["user"],
                "PASSWORD" => $config["password"]
            ];
        }

        if (file_exists($this->configuration["WP_CONFIG"])) {
            $this->connection = [
                "SERVER" => "",
                "DATABASE" => "",
                "USER" => "",
                "PASSWORD" => ""
            ];
        }

        if (count($this->connection) === 0) {
            $props = [
                "status" => "failed",
                "message" => "No sql configuration found!",
                "code" => 400
            ];

            return new JsonResponse($props);
        }

        if ((!isset($_GET["fields"]) || $_GET["fields"] === "") && (isset($_GET["type"]) && $_GET["type"] != "view")) {
            $props = [
                "status" => "failed",
                "message" => "Missing query string.",
                "code" => 400
            ];

            return new JsonResponse($props);
        }

        return false;
    }

    /**
     * Prepares the configuration.
     */
    public function prepareConfiguration()
    {
        $this->configuration["UPDATER_CONFIG"] = __DIR__ . "/../../.." . $this->configuration["UPDATER_CONFIG"];
        $this->configuration["TYPO_CONFIG"] = __DIR__ . "/../../../" . $this->webroot . $this->configuration["TYPO_CONFIG"];
        $this->configuration["WP_CONFIG"] = __DIR__ . "/../../../" . $this->webroot . $this->configuration["WP_CONFIG"];
    }

    /**
     * Runs the app.
     *
     * @return JsonResponse
     */
    public function run()
    {
        switch($this->configuration["type"]) {
            case "view":
                try {
                    $viewer = new DbTableInfo($this->connection);
                } catch (Exception $e) {
                    $props = [
                        "status" => "failed",
                        "message" => $e->getMessage(),
                        "code" => 400
                    ];

                    return new JsonResponse($props);
                }

                return $viewer->get();
            case "sql":
                try {
                    $updater = new DbReplicator($this->connection, $_GET["fields"]);
                } catch (Exception $e) {
                    $props = [
                        "status" => "failed",
                        "message" => $e->getMessage(),
                        "code" => 400
                    ];

                    return new JsonResponse($props);
                }

                return $updater->get();
            case "files":
                break;
            default:
                break;
        }
    }
}
