<?php

namespace Database;

use Exception;
use mysqli;
use Http\JsonResponse;

/**
 * Class UpdaterService
 */
class DbReplicator
{
    /** @var */
    private $mysqli;

    /** @var array */
    private $fields;

    /**
     * UpdaterService constructor.
     *
     * @param array $connection
     * @param string $fields
     * @throws Exception
     */
    public function __construct(array $connection, string $fields)
    {
        $this->mysqli = new mysqli(
            $connection['SERVER'], $connection['USER'], $connection['PASSWORD'], $connection['DATABASE']
        );

        if ($this->mysqli->connect_errno) {
            throw new Exception($this->mysqli->connect_error);
        }

        $this->mysqli->query("SET NAMES 'utf8'");

        $this->fields = explode(",", $fields);
    }

    /**
     * @return JsonResponse
     */
    public function get(): JsonResponse
    {
        $array = [];
        foreach ($this->fields as $field) {
            /** @noinspection SqlNoDataSourceInspection */
            $query = "SELECT * FROM " . $field;
            $result = $this->mysqli->query($query);

            $array = array_merge($array, [
                $field => []
            ]);

            while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                array_push($array[$field], $row);
            };
        }

        $props = [
            "status" => "success",
            "code" => 200,
            "data" => $array
        ];

        return new JsonResponse($props);
    }
}
