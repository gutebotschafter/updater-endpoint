<?php

namespace Database;

use Exception;
use mysqli;
use Http\JsonResponse;

/**
 * Class DbTableInfo
 * @package Database
 */
class DbTableInfo
{
    /** @var */
    private $mysqli;

    /** @var */
    private $database;

    /**
     * DbTableInfo constructor.
     *
     * @param array $connection
     * @throws Exception
     */
    public function __construct(array $connection)
    {
        $this->mysqli = new mysqli(
            $connection['SERVER'], $connection['USER'], $connection['PASSWORD'], $connection['DATABASE']
        );

        if ($this->mysqli->connect_errno) {
            throw new Exception($this->mysqli->connect_error);
        }

        $this->database = $connection["DATABASE"];

        $this->mysqli->query("SET NAMES 'utf8'");
    }

    /**
     * @return JsonResponse
     */
    public function get(): JsonResponse
    {
        $array = [];

        /** @noinspection SqlResolve */
        $query = "SELECT table_name FROM information_schema.tables WHERE table_type = 'base table' AND table_schema='" . $this->database . "'";
        $result = $this->mysqli->query($query);

        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            array_push($array, $row["table_name"]);
        };

        $props = [
            "status" => "success",
            "code" => 200,
            "data" => $array
        ];

        return new JsonResponse($props);
    }
}
