<?php /** @noinspection PhpIncludeInspection */

function my_autoloader($class_name)
{
    $file = __DIR__ . "/src/" . str_replace("\\", "/", $class_name) . ".php";

    if (file_exists($file)) {
        /** @noinspection PhpIncludeInspection */
        require_once($file);
    }
}

spl_autoload_register('my_autoloader');

use Updater\App;

$configuration = [
    "TYPO_CONFIG" =>  "/typo3conf/LocalConfiguration.php",
    "WP_CONFIG" => "/wp-config.php",
    "UPDATER_CONFIG" => "/updater-token.php",
    "type" => isset($_GET["type"]) && !empty($_GET["type"]) ? $_GET["type"] : "sql",
    "requestToken" => isset($_GET["token"]) && !empty($_GET["token"]) ? $_GET["token"] : null
];

$app = new App($configuration);
$app->run();
